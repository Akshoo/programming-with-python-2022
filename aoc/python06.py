with open("input06.txt","rt")as character_file:
    character_marker=character_file.read().strip() # Reading the four-letter markers without creating a list
#print(character_marker)
#print(len(character_marker)) #4095
for i in range(4,len(character_marker)):
    #print(i)
    next_char_marker=character_marker[(i-4):i] # moves 4 characters over the range starting from position and ending at last position of equal length
    # If [i:(i+4) given, the last characters are not four character marker of equal length]
    print(next_char_marker,i)
    unique_elements=set(next_char_marker) # Taking set to remove the duplicated elements present
    print(unique_elements)
    if len(unique_elements)==4: # As I want only one four-character marker, I am setting a condition to get out of the loop when the result is there
        first_four_character_marker=i
        print(i)
        break
#PART-TWO

for i in range(14,len(character_marker)):
    #print(i)
    next_char_marker=character_marker[(i-14):i] # moves 14 characters over the range starting from position and ending at last position of equal length
    #print(next_char_marker,i)
    unique_elements=set(next_char_marker) # Taking set to remove the duplicated elements present
    #print(unique_elements)
    if len(unique_elements)==14: # As I want only one fourteen-character marker, I am setting a condition to get out of the loop when the result is there
        first_fourteen_character_marker=i
        print(i)
        break

print(f"PART-1 Answer: The character needed to be processed before first start of packet marker is {first_four_character_marker}")
print(f"PART-2 Answer: The character needed to be processed before first start of packet marker is {first_fourteen_character_marker}")
    

    