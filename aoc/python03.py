
with open("input03.txt","rt")as items_in_Rucksack:
    packed_items=items_in_Rucksack.read().strip().split("\n")
    #print(packed_items)
#print(len(packed_items))
total_sum1=0

for items in packed_items:
    
    #print(list_of_items)
    #print(len(items))
    first_second_compartment=len(items)//2
    #print(first_second_compartment)
    first_half_compartment=items[:first_second_compartment]
    #print(first_half_compartment)
    second_half_compartment=items[first_second_compartment:]
    #print(items, first_half_compartment,second_half_compartment)
    #print(set(second_half_compartment))
    unique_elements=[]
    repeated_elements=[]
    for half in first_half_compartment:
      
    
     if half not in unique_elements:
      unique_elements.append(half)
     elif half not in repeated_elements:
      repeated_elements.append(half)
    #print(unique_elements)
    #print(repeated_elements)

    unique_elements2=[]
    repeated_elements2=[]
    for half in second_half_compartment:
        if half not in unique_elements2:
            unique_elements2.append(half)
        elif half not in repeated_elements2:
            repeated_elements2.append(half)
    #print(unique_elements2)
    #print(repeated_elements2)
    
    a_set = set(unique_elements)
    b_set = set(unique_elements2)
    if (a_set & b_set):
       pass
    else:
       print("No common elements")

    common_elements= list(a_set & b_set)
    #print(common_elements)
    for c in common_elements:
        if c.isupper():
            total_sum1+=ord(c)-ord('A')+27
        else:
            total_sum1+=ord(c)-ord('a')+1
    print(total_sum1) #Answer:8105

#PART-TWO

with open("input03.txt","rt")as items_in_Rucksack:
    packed_items=[items for items in items_in_Rucksack.read().strip().split("\n")]
print(type(packed_items))
total_sum2=0
for items in range(0,len(packed_items),3):
  rucksacks=packed_items[items:items+3]
  #print(rucksacks[0],rucksacks[1],rucksacks[2])
  c_set = set(rucksacks[0])
  d_set =set(rucksacks[1])
  e_set=set(rucksacks[2])
  #print(c_set,d_set,e_set)
  if (c_set & d_set & e_set):
    pass
  else:
    print("No common elements")
  common_elements= list(c_set & d_set & e_set)
  #print(common_elements)

  for c in common_elements:
        if c.isupper():
            total_sum2+=ord(c)-ord('A')+27
        else:
            total_sum2+=ord(c)-ord('a')+1
        print(total_sum2) # Ans:2363
  
    
print(f"Part1 answer according to the common elements is {total_sum1}")
print(f"Part2 answer according to the common elements is {total_sum2}")
    
    


    
    



