with open("input02.txt","rt")as Strategy_guide:
    guide= Strategy_guide.read().strip().split("\n")
#print(guide)



# Opponent:
   #A---- Rock
   #B---- Paper
   #C---- Scissor
# My chance:
   #X---- Rock
   #Y---- Paper
   #Z---- Scissor
# Outcome of that round: DRAW-3;WIN-6;LOSE-0
# Possible Combination involved with Opponent versus My Chance
#'AX','BX','CX','AY','BY','CY','AZ','BZ,'CZ'
#Probability for Score Outcome:
#AX= Rock and Rock---- DRAW----     1+3=4
#AY= Rock and Paper-----WIN-----    2+6=8
#AZ= Rock and Scissor--- LOSE-----   3+0=3
#BX= Paper and Rock--- LOSE----     1+0=1
#BY= Paper and Paper---- DRAW----   2+3=5
#BZ= Paper and Scissor--- WIN---   3+6=9
#CX= Scissor and Rock---WIN----     1+6=7
#CY= Scissor and Paper--- LOSE---   2+0=2
#CZ= Scissor and Scissor--- DRAW--- 3+3=6
Opponent_myChance=['A X','B X','C X','A Y','B Y','C Y','A Z','B Z','C Z']
Scores=[4,1,7,8,5,2,3,9,6]
#print(Opponent_myChance,Scores)
d_Opponent_myChance_Scores=dict(zip(Opponent_myChance,Scores))
#print(d_Opponent_myChance_Scores.items())
Sum_of_Scores=0
for elements in guide:
    print(elements,d_Opponent_myChance_Scores[elements])
    Sum_of_Scores+=d_Opponent_myChance_Scores[elements]
#print(Sum_of_Scores)



#Probability for Score Outcome:
#X=LOSE;Y=DRAW;Z=WIN

#AX= Rock and Rock---- LOSE----     3
#AY= Rock and Paper-----DRAW-----   4
#AZ= Rock and Scissor--- WIN-----   8
#BX= Paper and Rock--- LOSE----     1
#BY= Paper and Paper---- DRAW----   5
#BZ= Paper and Scissor--- WIN---    9
#CX= Scissor and Rock---LOSE----    2
#CY= Scissor and Paper--- DRAW---   6
#CZ= Scissor and Scissor--- WIN---  7 

Opponent_myChance=['A X','B X','C X','A Y','B Y','C Y','A Z','B Z','C Z']
Scores=[3,1,2,4,5,6,8,9,7]
d_Opponent_myChance_Scores=dict(zip(Opponent_myChance,Scores))
print(d_Opponent_myChance_Scores)
Sum_of_Scores_2=0
for elements in guide:
    print(elements)
    print(d_Opponent_myChance_Scores[elements])
    Sum_of_Scores_2+=d_Opponent_myChance_Scores[elements]
print(Sum_of_Scores_2)

print(f"Part1 answer according to the Strategy guide is {Sum_of_Scores}")
print(f"Part2 answer according to the Strategy guide is {Sum_of_Scores_2}")